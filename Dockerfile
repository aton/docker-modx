FROM php:5.6-apache

RUN apt-get update && \
	apt-get install -y \
	libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
	&& docker-php-ext-install -j$(nproc) iconv mcrypt \
    	&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    	&& docker-php-ext-install -j$(nproc) gd \
	&& docker-php-ext-install pdo_mysql mbstring

RUN  /usr/sbin/a2enmod rewrite

COPY modx-2.4.2-pl /var/www/html/
COPY config/php.ini /usr/local/etc/php/
COPY config/ports.conf /etc/apache2/
COPY config/apache2.conf /etc/apache2/ 
COPY config/000-default.conf /etc/apache2/sites-available/

RUN chown -R www-data:www-data /var/www/html

RUN /usr/sbin/a2ensite 000-default
