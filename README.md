MODX CMS over Docker
====================

Creates an image for MODX  over Docker image. Includes PHP 5.6 and Apache 2.

Image is based on [official PHP image on Docker hub](https://hub.docker.com/_/php/).

Version
-------

**Package version**: 1.0
**MODX version**: 2.4.2-pl

**Status:**: development

Usage
-----

This package includes service tool for building image - __build.sh__. Instructions are available running

```
./build.sh --help
```
It's recommended to run tool **as root** (or by __sudo ./build.sh__ on Ubuntu-like system.

Full sample: 

```
sudo ./build.sh --name modx -r -q 
```

Maintainer
----------

Alexander Pankov <pankov.ap@gmail.com>

